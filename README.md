# create react app

### This setup uses
- Create-React-App
- Typescript
- SCSS
- Redux
- DevTools
- ESLint
- UI components
- Router
- Jest
- Coverage
- SonarCloud
- CI pipeline
- Tailwind

### TBD

- prettier formatting
