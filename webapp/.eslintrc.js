module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    // 'plugin:@typescript-eslint/recommended',
    // 'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'standard',
    'prettier',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: 'module',
    project: './tsconfig.json',
  },
  plugins: [
    'react',
    'react-hooks',
    '@typescript-eslint'
  ],
  settings: {
    react: { version: 'detect' }
  },
  rules: {
    semi: ['error', 'always'],
    // note you must disable the base rule as it can report incorrect errors
    'no-shadow': 'off',
    'no-unused-vars': 'off',
    'no-undef': 'off',
    "no-use-before-define": "off",
    // end of base rules
    "@typescript-eslint/no-use-before-define": ["error"],
    'no-restricted-imports': [
      'error',
      {
        paths: [
          {
            name: 'react-redux',
            importNames: ['useSelector', 'useDispatch'],
            message: 'Use @/store instead',
          },
          {
            name: 'react-dom',
            importNames: ['render'],
            message: 'use TestContext.tsx instead'
          },
          {
            name: '@testing-library/react',
            importNames: ['render'],
            message: 'use TestContext.tsx instead'
          },
        ]
      }
    ]
  }
}
