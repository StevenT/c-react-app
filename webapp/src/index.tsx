import React from 'react';
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {BrowserRouter} from "react-router-dom";

import {App} from "./App";
import {initializeStore} from './store';
import './scss/tailwind.scss';
import './scss/index.scss';

const store = initializeStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
