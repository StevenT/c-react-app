import {Person} from "models/Person";
import {registerService} from "../injection";

export class StarWarsService {
  url = 'https://swapi.dev/api';

  loadPerson = async (id: number): Promise<Person> => {
    const res = await fetch(`${this.url}/people/${id}/`);
    if (!res.ok) {
      throw new Error(`Error loading Person: ${res.status}`);
    }
    // TODO: convert to model
    return await res.json() as Person;
  }
}

registerService(StarWarsService);