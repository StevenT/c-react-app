import { ServiceProvider } from './Injector';

export const ServiceKey = Symbol('this class is a service');

export const registerService = (service: ServiceProvider<unknown>): void => {
  Reflect.defineMetadata(ServiceKey, true, service);
  console.debug(`Registered service [${service.name}]`);
};
