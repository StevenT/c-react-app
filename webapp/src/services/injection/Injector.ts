import 'reflect-metadata';

import { ServiceKey } from './Service';

function isService<T>(func: Provider<T>): func is ServiceProvider<T> {
  return !!Reflect.getMetadata(ServiceKey, func);
}

export interface ServiceProvider<T> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  new (injector: Injector): T;
}

export interface FunctionProvider<T> {
  (injector: Injector): T;
}

export type Provider<T> = ServiceProvider<T> | FunctionProvider<T>;

export class Injector {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private readonly cache: Record<string, any> = {};
  public constructor() {
    this.bindTo<Injector>(Injector, this);
  }

  public bindTo<T>(target: Provider<T>, instance: T): void {
    this.cache[target.toString()] = instance;
  }

  public resolve<T>(target: Provider<T>): T {
    if (this.cache[target.toString()]) {
      return this.cache[target.toString()];
    }
    const instance = isService(target) ? this.resolveService(target) : this.resolveFunction(target);
    if (!instance) {
      throw new Error(`Failed to provide [${target.name}]`);
    }
    this.cache[target.toString()] = instance;
    return instance;
  }

  private resolveService<T>(target: ServiceProvider<T>): T {
    // eslint-disable-next-line new-cap
    const instance = new target(this.resolve(Injector));
    console.debug(`Initialized class ${target.name}`);
    return instance;
  }

  private resolveFunction<T>(target: FunctionProvider<T>): T {
    const instance = target(this.resolve(Injector));
    this.cache[target.name] = instance;
    console.debug(`Initialized function ${target.name}`);
    return instance;
  }
}