import React from "react";
import { Route, Switch } from "react-router-dom";
import { Navbar} from "components/Navbar";
import { HomePage } from "pages/HomePage";
import { AboutPage } from "pages/AboutPage";
import { NotFoundPage } from "pages/NotFoundPage";

export const App: React.FunctionComponent = () => {
  return (
    <>
      <Navbar />
      <div className="container">
        <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/about" component={AboutPage} />
        <Route component={NotFoundPage}/>
        </Switch>
      </div>
    </>
  );
};
