import React, {useContext} from 'react';
import {Injector, Provider} from 'services/injection/index';

const Context = React.createContext(new Injector());

export const InjectionProvider = Context.Provider;

export function useService<T>(provider: Provider<T>): T {
  return useContext(Context).resolve(provider);
}
