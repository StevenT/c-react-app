import React from "react";

export interface MyStyleSheet {
  [key: string]: React.CSSProperties;
}