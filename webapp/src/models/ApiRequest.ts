export class ApiEmpty {
  public loading = false;
  public empty = true;
  public value = null;
  public error = null;
}

export class ApiLoading {
  public loading = true;
  public empty = false;
  public value = null;
  public error = null;
}

export class ApiResult<T> {
  public loading = false;
  public empty = false;
  // eslint-disable-next-line no-useless-constructor
  constructor(public value: T) {}
  public error = null;
}

export class ApiError {
  public loading = false;
  public empty = false;
  public value = null;
  // eslint-disable-next-line no-useless-constructor
  constructor(public message: string, public error?: Object) {}
}

export type ApiRequest<T> = ApiEmpty | ApiLoading | ApiResult<T> | ApiError;
