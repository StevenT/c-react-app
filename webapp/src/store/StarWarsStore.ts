import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {ApiEmpty, ApiError, ApiLoading, ApiRequest, ApiResult} from "models/ApiRequest";
import { StarWarsService } from 'services/starwars/StarWarsService';
import { Person } from 'models/Person';

export interface State {
  person: ApiRequest<Person>;
}

const initialState: State = {
  person: new ApiEmpty(),
};

export const StarWarsStore = () => {
  const service: StarWarsService = new StarWarsService();

  const loadPerson = createAsyncThunk<ApiRequest<Person>, number,  { rejectValue: ApiError }>(
    'starWars/loadPerson',
    async (id: number, {rejectWithValue}) => {
      try {
        const person = await service.loadPerson(id);
        return new ApiResult<Person>(person);
      } catch (e) {
        console.error('Could not load Person', e);
        return rejectWithValue(new ApiError('Could not load Person', e.message));
      }
    }
  );

  const slice = createSlice({
    name: 'starWars',
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
      builder.addCase(loadPerson.pending, (state) => {
        state.person = new ApiLoading();
      });
      builder.addCase(loadPerson.fulfilled, (state, action) => {
        state.person = action.payload;
      });
      builder.addCase(loadPerson.rejected, (state, action) => {
        state.person = action.payload!;
      });
    },
  });

  const actions = {
    ...slice.actions,
    loadPerson,
  };

  const selectors = {
    person: (state: {
      starWars: State;
    }) => {
      return state.starWars.person;
    },
  };

  return {
    ...slice,
    initialState,
    actions,
    selectors,
  };
};
