import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';

// eslint-disable-next-line no-restricted-imports
import { useDispatch, useSelector } from 'react-redux';

import {composeWithDevTools} from "redux-devtools-extension";

import {SettingsStore} from './SettingsStore';
import {StarWarsStore} from './StarWarsStore';

export function initializeStore () {
  const middlewares = [thunkMiddleware];

  const rootReducer = combineReducers({
    settings: SettingsStore().reducer,
    starWars: StarWarsStore().reducer
  });

  const initialState = {
    settings: SettingsStore().initialState,
    starWars: StarWarsStore().initialState
  };

  const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middlewares)));

  return {
    ...store,
    /**
     * Equivalent to store.subscribe but the callback is only executed when the subState changes.
     * @param selector
     * @param callback
     */
    onUpdate<T> (
      selector: (state: ReturnType<typeof store['getState']>) => T,
      callback: (state: T) => void
    ) {
      // fetch initial state
      let previousState: T = selector(store.getState());
      store.subscribe(() => {
        const newState = selector(store.getState());
        if (previousState === newState) {
          // no update
          return;
        }
        previousState = newState;
        callback(newState);
      });
    }
  };
}

export type StoreType = ReturnType<typeof initializeStore>;
export type RootState = ReturnType<StoreType['getState']>;

export type AppDispatch = StoreType['dispatch'];
export const useAppDispatch = () => useDispatch<AppDispatch>();

export function useAppSelector<T> (selector: (state: RootState) => T) {
  return useSelector<RootState, T>(selector);
}
