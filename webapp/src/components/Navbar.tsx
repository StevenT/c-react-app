import React from "react";
import {Link} from "react-router-dom";
import './Navbar.scss';

export const Navbar: React.FunctionComponent = () => {
  return (
    <div className="nav">
      <nav>
        <ul>
          <li><Link to="/">home</Link></li>
          <li><Link to="/about">about</Link></li>
          <li><Link to="/NotFound">NotFound</Link></li>
        </ul>
      </nav>
      <div />
    </div>
  );
};
