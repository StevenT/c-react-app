import React, {ReactNode} from 'react';
// eslint-disable-next-line no-restricted-imports
import { render as rtlRender } from '@testing-library/react';
import { Provider } from 'react-redux';
import {Injector} from "services/injection";
import { initializeStore } from 'store';
import {InjectionProvider} from "hooks/useService";
import {MemoryRouter} from "react-router";

const injector = new Injector();

const render = (
  ui: JSX.Element,
  {
    initialState: RootState = {},
    store = injector.resolve(initializeStore),
    navigation = false,
    path = '/',
    ...renderOptions
  } = {}
) => {
  let wrapper;
  if (navigation) {
    // eslint-disable-next-line react/display-name
    wrapper = (props: { children?: ReactNode }) => {
      return (
        <Provider store={store}>
          <InjectionProvider value={injector}>
            <MemoryRouter initialEntries={[path]}>
              {props.children}
            </MemoryRouter>
          </InjectionProvider>
        </Provider>
      );
    };
  } else {
    // eslint-disable-next-line react/display-name
    wrapper = (props: { children?: ReactNode }) => {
      return (
        <Provider store={store}>
          <InjectionProvider value={injector}>
            {props.children}
          </InjectionProvider>
        </Provider>
      );
    };
  }
  return rtlRender(ui, { wrapper, ...renderOptions });
};

// eslint-disable-next-line no-restricted-imports
export * from '@testing-library/react';
export { render };
