import {MyStyleSheet} from "models/MyStyleSheet";
import React, {useState} from "react";
import {useAppDispatch, useAppSelector} from "store";
import {StarWarsStore} from 'store/StarWarsStore';
import {useService} from "../hooks/useService";

export const HomePage: React.FunctionComponent = () => {
  const dispatch = useAppDispatch();
  const {selectors, actions} = useService(StarWarsStore);
  const person = useAppSelector(selectors.person);
  const [id, setId] = useState('1');

  const loadPerson = () => {
    dispatch(actions.loadPerson(+id));
  };

  const styles: MyStyleSheet = {
    input: {
      width: 120
    }
  };

  return (
    <>
      <h1>Home page</h1>

      <button onClick={() => dispatch({type: 'myAction'})}>test</button>

      <input style={styles.input} placeholder='Person Id' value={id}
             onChange={(e) => setId(e.target.value)}/>
      <button color='teal' onClick={loadPerson}>load</button>
      {person.empty && (<div>Not loaded</div>)}
      {person.loading && (<div>Loading...</div>)}
      {person.error && (<div>{person.message} {JSON.stringify(person.error)}</div>)}
      {person.value && (<div>{person.value.name}</div>)}
    </>
  );
};
