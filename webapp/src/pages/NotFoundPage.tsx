import React from "react";

export const NotFoundPage: React.FunctionComponent = () => {
  return (
    <>
      <h1>Page not found</h1>
      <p>The page you requested cannot be found!</p>
    </>
  );
};
