import {ThunkAction} from 'redux-thunk';
import {Action} from 'redux';

declare module 'redux' {
  /*
   * Overload to add thunk support to Redux's dispatch() function.
   * Useful for react-redux or any other library which could use this type.
   *
   * See: https://github.com/reduxjs/redux-thunk/pull/247#issuecomment-521610736
   */

  // @ts-ignore
  export interface Dispatch<TBasicAction extends Action> {
    <TReturnType, TState, TExtraThunkArg>(
      thunkAction: ThunkAction<
        TReturnType,
        TState,
        TExtraThunkArg,
        TBasicAction
      >,
    ): TReturnType;
  }
}
