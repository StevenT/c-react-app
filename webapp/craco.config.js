module.exports = {
  jest: {
    configure: {
      "coverageDirectory": "coverage",
      "coveragePathIgnorePatterns": ["__tests__"],
      "testResultsProcessor": "jest-sonar-reporter",
      "cacheDirectory": ".jest/cache",
      "reporters": [
        "default",
        "jest-junit"
      ]
    }
  },
  style: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
};
